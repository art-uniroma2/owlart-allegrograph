package it.uniroma2.art.owlart.agraphimpl.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.models.TransactionBasedModel;
import it.uniroma2.art.owlart.models.impl.ObjectsOfStatementsIterator;
import it.uniroma2.art.owlart.models.impl.ResourceIteratorWrappingNodeIterator;
import it.uniroma2.art.owlart.models.impl.SKOSXLModelImpl;
import it.uniroma2.art.owlart.models.impl.SubjectsOfStatementsIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorWrappingNodeIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorWrappingResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.sesame2impl.models.BaseRDFModelSesame2Impl;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;

/**
 * <em>Note:</em> All the direct reasoning has been modified with respect to Sesame2 implementation, by simply
 * using explicit triples with standard RDF/RDFS predicates
 * 
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it><br/>
 *         Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class SKOSXLModelAllegroGraphImpl extends SKOSXLModelImpl implements DirectReasoning, TransactionBasedModel {

	public SKOSXLModelAllegroGraphImpl(BaseRDFModelSesame2Impl baseRep) {
		super(baseRep);
	}

	public ARTResourceIterator listDirectInstances(ARTResource type, ARTResource... contexts)
			throws ModelAccessException {
		// return new SubjectsOfStatementsIterator(listStatements(null, SESAME.Res.DIRECTTYPE, type, true,
		// contexts));
		return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, RDF.Res.TYPE, type, false,
				contexts));
	}

	public ARTResourceIterator listDirectTypes(ARTResource res, ARTResource... contexts)
			throws ModelAccessException {
		// return new ResourceIteratorWrappingNodeIterator( new
		// ObjectsOfStatementsIterator(listStatements(res, SESAME.Res.DIRECTTYPE, null, true, contexts)) );
		return new ResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(res,
				RDF.Res.TYPE, NodeFilters.ANY, false, contexts)));
	}

	public ARTResourceIterator listDirectSuperClasses(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		// return new ResourceIteratorWrappingNodeIterator( new
		// ObjectsOfStatementsIterator(listStatements(resource, SESAME.Res.DIRECTSUBCLASSOF, null, true,
		// contexts)) );
		return new ResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(
				resource, RDFS.Res.SUBCLASSOF, NodeFilters.ANY, false, contexts)));
	}

	public ARTURIResourceIterator listDirectSuperProperties(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(listStatements(
				resource, RDFS.Res.SUBPROPERTYOF, NodeFilters.ANY, false, contexts)));
		// return new URIResourceIteratorWrappingNodeIterator( new
		// ObjectsOfStatementsIterator(listStatements(resource, SESAME.Res.DIRECTSUBPROPERTYOF, null, true,
		// contexts)) );
	}

	public ARTResourceIterator listDirectSubClasses(ARTResource resource, ARTResource... contexts)
			throws ModelAccessException {
		// return new SubjectsOfStatementsIterator(listStatements(null, SESAME.Res.DIRECTSUBCLASSOF, resource,
		// true, contexts));
		return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, RDFS.Res.SUBCLASSOF,
				resource, false, contexts));
	}

	public ARTURIResourceIterator listDirectSubProperties(ARTURIResource resource, ARTResource... contexts)
			throws ModelAccessException {
		// return new URIResourceIteratorWrappingResourceIterator( new
		// SubjectsOfStatementsIterator(listStatements(null, SESAME.Res.DIRECTSUBPROPERTYOF, resource, true,
		// contexts)) );
		return new URIResourceIteratorWrappingResourceIterator(new SubjectsOfStatementsIterator(
				listStatements(NodeFilters.ANY, RDFS.Res.SUBPROPERTYOF, resource, false, contexts)));
	}

	public void setAutoCommit(boolean value) throws ModelUpdateException {
		((BaseRDFModelSesame2Impl)baseRep).setAutoCommit(value);
		
	}

	public boolean isAutoCommit() throws ModelAccessException {
		return ((BaseRDFModelSesame2Impl)baseRep).isAutoCommit();
	}

	public void rollBack() throws ModelAccessException {
		((BaseRDFModelSesame2Impl)baseRep).rollBack();		
	}

	public void commit() throws ModelUpdateException {
		((BaseRDFModelSesame2Impl)baseRep).commit();
	}
}