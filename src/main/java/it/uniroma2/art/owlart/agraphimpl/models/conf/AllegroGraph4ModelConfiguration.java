package it.uniroma2.art.owlart.agraphimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;
import it.uniroma2.art.owlart.models.conf.RemoteModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.RequiredConfigurationParameter;

public class AllegroGraph4ModelConfiguration extends RemoteModelConfigurationImpl implements PersistenceModelConfiguration {

	public AllegroGraph4ModelConfiguration() {
		username = USERNAME;
		password = PASSWORD;
	}

	@ModelConfigurationParameter(description = "ID of the AllegroGraph catalog to be connected")
	@RequiredConfigurationParameter
	public String agCatalogId = ROOT_CATALOG_ID;

	@ModelConfigurationParameter(description = "repositoryID of the AllegroGraph repository to be connected")
	@RequiredConfigurationParameter
	public String agRepositoryId;

	@ModelConfigurationParameter(description = "true if the allegrograph repository has to support RDFS inferencing")
	public boolean rdfsInference = true;

	public static final String JAVA_CATALOG_ID = "java-catalog";
	public static final String ROOT_CATALOG_ID = "Root";

	private static final String USERNAME = "test";
	private static final String PASSWORD = "xyzzy";

	public String getShortName() {
		return "standard remote conf";
	}

	public boolean isPersistent() {
		return true;
	}

}
