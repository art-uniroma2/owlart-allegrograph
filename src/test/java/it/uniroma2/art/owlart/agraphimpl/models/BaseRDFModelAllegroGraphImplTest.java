package it.uniroma2.art.owlart.agraphimpl.models;


import it.uniroma2.art.owlart.agraphimpl.factory.ARTModelFactoryAllegroGraphImpl;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import java.io.File;

/**
 * basic AllegroGraph implementation for unit tests of OWL ART API
 * 
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 *
 */
public class BaseRDFModelAllegroGraphImplTest extends BaseRDFModelTest {
	
	@BeforeClass
	public static void loadRepository() throws Exception {
		BaseRDFModelTest.initializeTest(new ARTModelFactoryAllegroGraphImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "agraph");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
	
}
