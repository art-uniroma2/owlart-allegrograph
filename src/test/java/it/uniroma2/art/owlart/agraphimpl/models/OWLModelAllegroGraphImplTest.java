package it.uniroma2.art.owlart.agraphimpl.models;

import it.uniroma2.art.owlart.agraphimpl.factory.ARTModelFactoryAllegroGraphImpl;
import it.uniroma2.art.owlart.agraphimpl.models.conf.AllegroGraph4ModelConfiguration;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModelTest;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.vocabulary.SESAME;

import java.io.File;
import java.util.Collection;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class OWLModelAllegroGraphImplTest extends OWLModelTest {
	@BeforeClass
	public static void loadRepository() throws Exception {

		ARTModelFactoryAllegroGraphImpl fact = new ARTModelFactoryAllegroGraphImpl();
		
		ModelConfiguration mcfg = fact.createModelConfigurationObject(AllegroGraph4ModelConfiguration.class);
		mcfg.loadParameters(new File("config/testmodel.config"));
		
		
		OWLModelTest.initializeTest(new ARTModelFactoryAllegroGraphImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
/*			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "agraph");
			deleteDir(memStoreFile.getAbsoluteFile());
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
*/			
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
	
	
	protected void addTripleStoreSpecificNamedResources(Collection<ARTURIResource> expectedNamedResources) {
		expectedNamedResources.add(SESAME.Res.DIRECTSUBCLASSOF);
		expectedNamedResources.add(SESAME.Res.DIRECTSUBPROPERTYOF);
		expectedNamedResources.add(SESAME.Res.DIRECTTYPE);
	}
	
	// Deletes all files and subdirectories under dir.
	// Returns true if all deletions were successful.
	// If a deletion fails, the method stops attempting to delete and returns false.
	public static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	}
}

