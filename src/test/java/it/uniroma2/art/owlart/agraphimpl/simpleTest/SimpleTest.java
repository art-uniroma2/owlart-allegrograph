package it.uniroma2.art.owlart.agraphimpl.simpleTest;

import it.uniroma2.art.owlart.agraphimpl.factory.ARTModelFactoryAllegroGraphImpl;
import it.uniroma2.art.owlart.agraphimpl.models.conf.AllegroGraph4ModelConfiguration;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

public class SimpleTest {

	public static void main(String[] args) throws ModelCreationException, ModelAccessException,
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException {
		ARTModelFactoryAllegroGraphImpl factImpl = new ARTModelFactoryAllegroGraphImpl();

		OWLArtModelFactory<AllegroGraph4ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		AllegroGraph4ModelConfiguration conf = fact.createModelConfigurationObject();
		conf.serverURL = "http://192.168.204.128:10035";
		SKOSXLModel model = fact.loadModel(SKOSXLModel.class, "http://aims.fao.org/aos/agrovoc", ".", conf);

		ARTURIResourceIterator it = model.listConceptsInScheme(model
				.createURIResource("http://aims.fao.org/aos/agrovoc/agrovocScheme"));
		int counter = 0;
		while (it.streamOpen()) {
			System.out.println(it.getNext().getLocalName());
			counter++;
		}
		System.out.println("for a total of: " + counter + " concepts");
	}

}
