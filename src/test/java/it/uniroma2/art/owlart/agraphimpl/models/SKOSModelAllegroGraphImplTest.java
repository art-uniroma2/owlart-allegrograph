package it.uniroma2.art.owlart.agraphimpl.models;

import it.uniroma2.art.owlart.agraphimpl.factory.ARTModelFactoryAllegroGraphImpl;
import it.uniroma2.art.owlart.agraphimpl.models.conf.AllegroGraph4ModelConfiguration;
import it.uniroma2.art.owlart.models.SKOSModelTest;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openrdf.repository.RepositoryException;

import com.franz.agraph.repository.AGCatalog;
import com.franz.agraph.repository.AGServer;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 * 
 */
public class SKOSModelAllegroGraphImplTest extends SKOSModelTest {

	@BeforeClass
	public static void loadRepository() throws Exception {
		SKOSModelTest.initializeTest(new ARTModelFactoryAllegroGraphImpl(), false);
	}

	@AfterClass
	public static void classTearDown() throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		ARTModelFactoryAllegroGraphImpl fact = new ARTModelFactoryAllegroGraphImpl();
		AllegroGraph4ModelConfiguration modConf = fact
				.createModelConfigurationObject(AllegroGraph4ModelConfiguration.class);
		AGServer server = new AGServer(modConf.serverURL, modConf.username, modConf.password);
		AGCatalog catalog = server.getRootCatalog(); // open rootCatalog
		String repositoryId = modConf.agRepositoryId;
		try {
			catalog.deleteRepository(repositoryId);
			System.out.println("repository deleted");
			System.out.println("-- test teared down --");
		} catch (RepositoryException e) {
			System.err.println("failed to close the repository");
		}
	}

}